# Bookmarks
The bookmarks bar wasn't cutting it for me...

I got tired of trying to keep all of my bookmarks updated and between two machines it was getting weird.
Opting for this route instead.

## VLANs with Unifi Dream Machine
https://digitalaidonline.com/2020/09/06/vlan-and-firewall-setup-for-network-segmentaton-on-unifi-dream-machine-udm/

## Body Classes within WordPress
https://webdesign.tutsplus.com/tutorials/adding-to-the-body-class-in-wordpress--cms-21077

## Case Study: ONLINE EDUCATION PARTNER
https://www.pageonepower.com/seo-case-study-online-education

## How to Scale Title Tag and Meta Description Creation
https://www.pageonepower.com/linkarati/scale-title-tag-meta-description-creation

## Page One Power SEO Blog
https://www.pageonepower.com/linkarati

## Sample Breakdowns
https://www.tracklib.com/blog/sample-breakdowns/

## Hours and Days Analytics Reports
http://barker.co.uk/gahours

## Create Custom Reports in Analytics
https://www.monsterinsights.com/how-to-create-custom-reports-in-google-analytics-step-by-step/

## 3 Types Google Analytics Reports
https://www.digitalmarketer.com/blog/best-google-analytics-reports/

## RSS Feed Parser
https://css-tricks.com/how-to-fetch-and-parse-rss-feeds-in-javascript/

## Pa11y
https://pa11y.org/

## Mozilla Dev CSS Attribute Selectors
https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors

## Find Penguins
https://findpenguins.com/

## CycleBlaze
https://www.cycleblaze.com/

## Kirki WordPress Sample:
https://helpwp.com/add-homepage-banner-wordpress-customizer/

## United States Web Design System
https://designsystem.digital.gov/how-to-use-uswds/

## Yale Usability Guide
https://usability.yale.edu/

## Web Style guides
http://styleguides.io/

## Responsive Patterns (Brad Frost)
http://bradfrost.github.io/this-is-responsive/patterns.html

## Colorable
https://jxnblk.github.io/colorable/demos/text/

## A11Y Colors
http://clrs.cc/a11y/

## WAI-ARIA
https://rawgit.com/w3c/aria/master/

## Managing Flow and Rhythm with CSS Custom Properties
https://24ways.org/2018/managing-flow-and-rhythm-with-css-custom-properties/

## Axiomatic CSS and Lobotomized Owls
https://alistapart.com/article/axiomatic-css-and-lobotomized-owls/

## Contrast Ratio
https://contrast-ratio.com/

## EightShape Contrast Grid
https://contrast-grid.eightshapes.com/

## Boilerform
https://boilerform.hankchizljaw.com/

## Modern CSS Reset
https://github.com/hankchizljaw/modern-css-reset

## Uninstall Facebook
https://npm-uninstall-facebook.com/

## MyBrowser FYI
https://mybrowser.fyi/

## Every Damn Layout
https://every-layout.dev/

## Select and copy data to clipboard using jQuery
https://www.coderomeos.org/select-and-copy-data-to-clipboard-using-jquery

## Free Mockup Templates
https://mockup.photos/freebies?filter=any,free,any,popular&page=1

## MyStock Photos
https://mystock.themeisle.com/

## Expanding the height of a Flickity slide when height of content changes
https://stackoverflow.com/questions/51949631/expanding-the-height-of-a-flickity-slide-when-height-of-content-changes

## Autoptimize | Inline Defer Render Blocking CSS | Page Specific Above-The-Fold CSS
https://aoxoa.co/inline-defer-render-blocking-css-autoptimize/

## ASCII FLOW
http://asciiflow.com/

## Critical Path CSS Generator
https://jonassebastianohlsson.com/criticalpathcssgenerator/

## WordPress Hash Generator
http://www.passwordtool.hu/wordpress-password-hash-generator-v3-v4

## Drop-Down Navigation with CSS only
https://css-snippets.com/drop-down-navigation/

## Matt Griffin
https://matt-griffin.com/

## Framer
https://www.framer.com/

## React Course for Designers
https://react.design/

## Stackable Blocks for Gutenberg
https://wpstackable.com/

## History and effective use of Vim
https://begriffs.com/posts/2019-07-19-history-use-vim.html

## ngrok with MAMP Pro
https://stackoverflow.com/questions/43800382/ngrok-not-working-with-mamp

## Google Search Operators
http://www.googleguide.com/advanced_operators_reference.html

## Unsplash Photos
https://unsplash.com/
